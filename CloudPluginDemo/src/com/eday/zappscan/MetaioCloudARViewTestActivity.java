package com.eday.zappscan;

import java.io.IOException;
import java.util.Date;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.view.WindowManager;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.eday.zappscan.CouponListView;
import com.metaio.cloud.plugin.MetaioCloudPlugin;
import com.metaio.cloud.plugin.arel.ARELWebView;
import com.metaio.cloud.plugin.util.MetaioCloudUtils;
import com.metaio.cloud.plugin.view.MetaioCloudARViewActivity;
import com.metaio.sdk.jni.DataSourceEvent;
import com.metaio.sdk.jni.IGeometry;
import com.metaio.sdk.jni.LLACoordinate;
import com.metaio.sdk.jni.MetaioWorldPOI;
import com.metaio.sdk.jni.Vector3d;

public class MetaioCloudARViewTestActivity extends MetaioCloudARViewActivity {
	/**
	 * GUI overlay
	 */
	private RelativeLayout mGUIView;

	/**
	 * Progress bar view
	 */
	private ProgressBar progressView;
	
	private View helpView;
	private View aboutView;
    
	
	private ARELWebView webView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Window managed wake lock (no permissions, no accidentally kept on)
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// Optionaly add GUI
		if (mGUIView == null)
			mGUIView = (RelativeLayout) getLayoutInflater().inflate(R.layout.arview, null);

		progressView = (ProgressBar) mGUIView.findViewById(R.id.progressBar);

		//Init the AREL webview. Pass a container if you want to use a ViewPager or Horizontal Scroll View over the camera preview or the root view. 
		webView = initARELWebView(mGUIView);
		//webView.getSettings().setJavaScriptEnabled(true);
		//anil
		/*final AndroidBridge myJavaScriptInterface
     	 new AndroidBridge(this);*/
		webView.addJavascriptInterface(new AndroidBridge(this), "Bridge");
		// webView.loadUrl("http://localhost:8080");
       
		//Used to resume the camera preview
		mIsInLiveMode = true;

	}
	
	

	@Override
	public void onStart() {
		super.onStart();

		SplashActivity.log("JunaioARViewTestActivity.onCreate()");

		//if we want to catch the sensor listeners
		MetaioCloudPlugin.getSensorsManager(getApplicationContext()).registerCallback(this);

		// add GUI layout
		if (mGUIView != null)
			addContentView(mGUIView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		//comes from splash activity
		final int channelID = getIntent().getIntExtra(getPackageName() + ".CHANNELID", -1);
		if (channelID > 0) {
			// Clear the intent extra before proceeding
			getIntent().removeExtra(getPackageName() + ".CHANNELID");
			setChannel(channelID);

		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		ImageView helpbtn = (ImageView) findViewById(R.id.helpButton);
		if(helpbtn != null)
			helpbtn.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public void onLocationSensorChanged(LLACoordinate location) {
		super.onLocationSensorChanged(location);
	}

	@Override
	public void onHeadingSensorChanged(float[] orientation) {
		super.onHeadingSensorChanged(orientation);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		/*
		if(helpView.isActivated())
		{
			helpView.setVisibility(View.GONE);
			showHelp(webView);
		}*/
		ImageView img = (ImageView) findViewById(R.id.helpImage);
		ImageView img2 = (ImageView) findViewById(R.id.splashImage);

	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	    	if(img != null)
	    	{		img.setImageResource(R.drawable.helpscreen_landscape);
	    			img.invalidate();
	    			/*Drawable drawable = this.getResources().getDrawable(R.drawable.helpscreen_landscape);
	    			img.setImageDrawable(drawable);*/
	    	}
	    	if(img2 != null)
	    		img2.setImageResource(R.drawable.splash_landscape);
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	    	if(img != null)
	    	{
	    			img.setImageResource(R.drawable.helpscreentrans);
	    			img.invalidate();
	    			/*Drawable drawable = this.getResources().getDrawable(R.drawable.helpscreentrans);
	    			img.setImageDrawable(drawable);*/
	    	}
	    	if(img2 != null)
	    		img2.setImageResource(R.drawable.splash);
	    }
	 
	    if(helpView != null)
	    	helpView.invalidate();
	}

	@Override
	public void onSurfaceChanged(int width, int height) {
		//always call the super implementation first
		super.onSurfaceChanged(width, height);

		//get radar margins from the resources (this will make the values density independant)
		float marginTop = getResources().getDimension(R.dimen.radarTop);
		float marginRight = getResources().getDimension(R.dimen.radarRight);
		float radarScale = getResources().getDimension(R.dimen.radarScale);
		//set the radar to the top right corner and add some margin, scale to 1
		setRadarProperties(IGeometry.ANCHOR_TOP | IGeometry.ANCHOR_RIGHT, new Vector3d(-marginRight, -marginTop, 0f), new Vector3d(radarScale, radarScale, 1f));
	}

	@Override
	protected void updateGUI() {
		// TODO: here update any GUI related to channel information, e.g. icon, name etc 
	}

	@Override
	public void onScreenshot(Bitmap bitmap) {
		// this is triggered by calling takeScreenshot() or through AREL
		String filename = "junaio-" + DateFormat.format("yyyyMMdd-hhmmss", new Date()) + ".jpg";

		try {
			boolean result = MetaioCloudUtils.writeToFile(bitmap, CompressFormat.JPEG, 100, MetaioCloudPlugin.mCacheDir, filename, false);

			if (result) {
				if (!saveToGalleryWithoutDialogFlag) {
					//show share view
					Intent intent = new Intent(getApplicationContext(), ShareViewActivity.class);
					intent.putExtra(getPackageName() + ".FILEPATH", MetaioCloudPlugin.mCacheDir + "/" + filename);
					startActivity(intent);
				} else {
					//save the screenshot to the gallery directly
					MetaioCloudUtils.saveScreenshot(bitmap, filename, this);
				}
			}
		} catch (IOException e) {
			Log.e("MetaioCloudARView", "Error formatting date", e);
		} catch (Exception e) {
			Log.e("MetaioCloudARView", "Error formatting date", e);
		}
	}

	@Override
	protected void showProgress(final boolean show) {

		progressView.post(new Runnable() {

			public void run() {
				progressView.setIndeterminate(true);
				progressView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
			}
		});
	}

	@Override
	protected void showProgressBar(final float progress, final boolean show) {

		progressView.post(new Runnable() {

			@Override
			public void run() {
				progressView.setIndeterminate(false);
				progressView.setProgress((int) progress);
				progressView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

			}
		});
	}

	@Override
	protected void onServerEvent(DataSourceEvent event) {
		switch (event) {
		case DataSourceEventNoPoisReturned:
			MetaioCloudUtils.showToast(this, getString(R.string.MSGI_POIS_NOT_FOUND));
			break;
		case DataSourceEventServerError:
			MetaioCloudUtils.showToast(this, getString(R.string.MSGE_TRY_AGAIN));
			break;
		case DataSourceEventServerNotReachable:
		case DataSourceEventCouldNotResolveServer:
			MetaioCloudUtils.showToast(this, getString(R.string.MSGW_SERVER_UNREACHABLE));
			break;
		default:
			break;
		}
	}

	@Override
	protected void onSceneReady() {
		super.onSceneReady();
	}

	@Override
	protected void onObjectAdded(MetaioWorldPOI object) {
		super.onObjectAdded(object);
	}

	@Override
	protected void onObjectRemoved(MetaioWorldPOI object) {
		super.onObjectRemoved(object);
	}

	@Override
	protected void onRemoveContent() {
		super.onRemoveContent();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	public void showHelp(View v) {
		/*
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			helpView = getLayoutInflater().inflate(R.layout.helpscreenlandscape, null);
		}
		else
		{*/
			helpView = getLayoutInflater().inflate(R.layout.helpscreen, null);
		/*}
		*/
		
		addContentView(helpView,  new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.MATCH_PARENT));
		
		
		ImageView img = (ImageView) findViewById(R.id.helpImage);
		if(img != null)
		{
			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			{
				img.setImageResource(R.drawable.helpscreen_landscape);
    			/*Drawable drawable = this.getResources().getDrawable(R.drawable.helpscreen_landscape);
    			img.setImageDrawable(drawable);*/
			}
			else
			{
				img.setImageResource(R.drawable.helpscreentrans);
    			/*Drawable drawable = this.getResources().getDrawable(R.drawable.helpscreentrans);
    			img.setImageDrawable(drawable);*/
			}
			img.invalidate();
		}
		
		helpView.invalidate();
	
		ImageView helpbtn = (ImageView) findViewById(R.id.helpButton);
		helpbtn.setVisibility(View.GONE);
		
	}
	
	public void showAbout(View v) {
		aboutView = getLayoutInflater().inflate(R.layout.about, null);
		addContentView(aboutView,  new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));	
		TextView noteView = (TextView) findViewById(R.id.aboutTextView);
		Linkify.addLinks(noteView, Linkify.ALL);
	}
	
	public void helpok(View v) {
		/*if(helpView != null)
			helpView.setVisibility(View.GONE);*/
		((ViewManager)helpView.getParent()).removeView(helpView);
		ImageView helpbtn = (ImageView) findViewById(R.id.helpButton);
		helpbtn.setVisibility(View.VISIBLE);
	}
	
	public void aboutok(View v) {
		((ViewManager)aboutView.getParent()).removeView(aboutView);
		helpok(v);
	}
	
	public void gotoEday(View v) {
		Uri uriUrl = Uri.parse("http://www.edaysolutions.com");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
	}
	
	public void Save(View v)
	{
		 Intent i = new Intent(getApplicationContext(), CouponListView.class);
         startActivity(i);
		
	}
	
	protected void hideImage() {

		webView.post(new Runnable() {
			public void run() {
				ImageView prompt = (ImageView) findViewById(R.id.pointPrompt);
    	     	if(prompt != null)
    	 		prompt.setVisibility(View.INVISIBLE);
			}
		});
	}
	
	protected void visibleImage() {

		webView.post(new Runnable() {
			public void run() {
				ImageView prompt = (ImageView) findViewById(R.id.pointPrompt);
    	     	if(prompt != null)
    	 		prompt.setVisibility(View.VISIBLE);
			}
		});
	}
}
