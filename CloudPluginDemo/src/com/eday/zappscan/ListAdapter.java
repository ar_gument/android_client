package com.eday.zappscan;

import android.widget.ArrayAdapter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends ArrayAdapter<Coupon> {


	private static final String tag = "CouponListAdapter";
	
	private Context context;
	private ImageView couponIcon;
	private TextView couponVendor;
	private TextView couponExpDate;
	private List<Coupon> coupons = new ArrayList<Coupon>();

	public ListAdapter(Context context, int textViewResourceId,
			List<Coupon> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.coupons = objects;
	}

	public int getCount() {
		return this.coupons.size();
	}

	public Coupon getItem(int index) {
		return this.coupons.get(index);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			// ROW INFLATION
			Log.d(tag, "Starting XML Row Inflation ... ");
			LayoutInflater inflater = (LayoutInflater) this.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.list_item, parent, false);
			Log.d(tag, "Successfully completed XML Row Inflation!");
		}

		// Get item
		Coupon coupon = getItem(position);

		// Get reference to ImageView 
		couponIcon = (ImageView) row.findViewById(R.id.coupon_thumb);

		// Get reference to TextView - country_name
		couponVendor = (TextView) row.findViewById(R.id.vendor);

		// Get reference to TextView - country_abbrev
		couponExpDate = (TextView) row.findViewById(R.id.exp_date);

		//Set vendor name
		couponVendor.setText(coupon.vendor);
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String date = sdf.format(coupon.expdate);
		couponExpDate.setText(date);

		// Set coupon bitmap icon using File path
		String imgFilePath = coupon.bmpResourceId;
		try {
			Bitmap bitmap = BitmapFactory.decodeStream(this.context.getResources().getAssets()
					.open(imgFilePath));
			couponIcon.setImageBitmap(bitmap);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return row;
	}
}
