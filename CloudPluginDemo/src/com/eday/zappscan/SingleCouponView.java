package com.eday.zappscan;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

public class SingleCouponView extends Activity {
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.single_coupon_view);
 
        ImageView couponImg = (ImageView) findViewById(R.id.single_coupon);
 
        Intent i = getIntent();
        String imgFilePath = i.getStringExtra("image");
		try {
			Bitmap bitmap = BitmapFactory.decodeStream(getApplicationContext().getResources().getAssets()
					.open(imgFilePath));
			couponImg.setImageBitmap(bitmap);
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
    }
}
