package com.eday.zappscan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class AndroidBridge  {
	private MetaioCloudARViewTestActivity activity;

    public AndroidBridge(MetaioCloudARViewTestActivity activity) {
        this.activity = activity;
    }
   public void couponSave() 
   {
	   System.out.println("I am here");
	   HttpClient httpclient = new DefaultHttpClient();
       String result = null;
	    // Prepare a request object
       
       // Get Connection
	    HttpGet httpget = new HttpGet("http://argument.protomo.fi/juna/rantest/202958/ilves3.png"); 
		
	    // Post Connection
	   // HttpPost httpPost = new HttpPost("http://91.155.178.24:3000/analytics/create");
	    
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
	    nameValuePairs.add(new BasicNameValuePair("advertisement_id", "e1088e34d6e3d96ba10806380b949e76"));
	    nameValuePairs.add(new BasicNameValuePair("action_type", "1"));
	    nameValuePairs.add(new BasicNameValuePair("occurred_when", "2013/6/18 11:45"));
	    

	    HttpResponse response;
	    
	    try {
	    	//httpget.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        response = httpclient.execute(httpget);
	        // Examine the response status
	        Log.i("anil",response.getStatusLine().toString());
	      // Toast.makeText(this,response.getStatusLine().toString() , Toast.LENGTH_LONG).show();
	        // Get hold of the response entity
	        HttpEntity entity = response.getEntity();
	        // If the response does not enclose an entity, there is no need
	        // to worry about connection release

	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	             result= convertStreamToString(instream);
	          // From result we can extract json parameters such as start_date, end_date
	          // and pass on to sqlite database  using Databasecreator class.
	             // eg.
	             /*
	               db.addValue( new Coupondata(mText1.getText().toString() could be replaced with json parameters,
       		   mText2.getText().toString()));
	             */
	           // Toast.makeText(this,result , Toast.LENGTH_LONG).show();
	            
	            // now you have the string representation of the HTML request
	            instream.close();
	           
	        }

          
	    } catch (Exception e) {
	    	
	    }  
  
		
	}
	
	 private static String convertStreamToString(InputStream is) {
		    /*
		     * To convert the InputStream to String we use the BufferedReader.readLine()
		     * method. We iterate until the BufferedReader return null which means
		     * there's no more data to read. Each line will appended to a StringBuilder
		     * and returned as String.
		     */
		    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		    StringBuilder sb = new StringBuilder();

		    String line = null;
		    try {
		        while ((line = reader.readLine()) != null) {
		            sb.append(line + "\n");
		        }
		    } catch (IOException e) {
		        e.printStackTrace();
		    } finally {
		        try {
		            is.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		    return sb.toString();
		}
   

    public void imageRecognised() {
    	
    	activity.hideImage();
    	
    	}
    
    public void imageLost() {
    	
    	activity.visibleImage();
    }


}
