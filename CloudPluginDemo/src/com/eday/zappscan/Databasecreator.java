package com.eday.zappscan;



import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.*;
import android.util.Log;
import android.widget.Toast;


public class Databasecreator extends SQLiteOpenHelper {
	
	private static final int DATABASE_VERSION = 1;
	 
    // Database Name
    private static final String DATABASE_NAME = "couponManager";
 
    // Contacts table name
    private static final String TABLE_COUPON = "coupon_data";
 
    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_START = "start_date";
    private static final String KEY_END = "end_date";
 

	public Databasecreator(Context context) {
		  super(context, DATABASE_NAME, null,DATABASE_VERSION); 
		  }

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		 String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_COUPON + "("
	                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_START + " TEXT,"
	                + KEY_END + " TEXT" + ")";
	        db.execSQL(CREATE_CONTACTS_TABLE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		 db.execSQL("DROP TABLE IF EXISTS " + TABLE_COUPON);
		 
	        // Create tables again
	        onCreate(db);
		
	}

	public void addValue(Coupondata data) {
		// TODO Auto-generated method stub
		
		
		 
	    ContentValues values = new ContentValues();
	    
	    String selectQuery = "SELECT  * FROM " + TABLE_COUPON;
		 
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	   
	    values.put(KEY_START, data.getStartDate()); // Contact Name
	    values.put(KEY_END, data.getEndDate()); // Contact Phone Number
	        
	 
	    // Inserting Row
	    
	    long ret = db.insert(TABLE_COUPON, null, values);
	    if (ret != -1)
	    {
	     System.out.println(" anil --sucess "); 
	    }
	    cursor.close();
	    db.close(); // Closing database connection
	}
	
	public Coupondata readValue() {
		// TODO Auto-generated method stub
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor= null;
		
		if (db!=null)
		{
	     cursor = db.query(TABLE_COUPON, null,null, null, null,null, null);
		}
	    
	    if (cursor != null)
	    {
	        cursor.moveToFirst();
	    }
	    Coupondata contact = new Coupondata(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
       
	    // return contact
	    db.close(); 
	    return contact; 
	   // Closing database connection		
	}
	
	public List<Coupondata> getAllCoupons() {
	    List<Coupondata> contactList = new ArrayList<Coupondata>();
	    // Select All Query
	    String selectQuery = "SELECT  * FROM " + TABLE_COUPON;
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (cursor.moveToFirst()) {
	        do {
	            Coupondata data = new Coupondata();
	            data.setID(Integer.parseInt(cursor.getString(0)));
	            data.setStartDate(cursor.getString(1));
	            data.setEndDate(cursor.getString(2));
	            // Adding contact to list
	            contactList.add(data);
	        } while (cursor.moveToNext());
	    }
	     cursor.close();
	     db.close();
	    // return contact list
	    return contactList;
	}
	
	public int getCoupondataCount() {
		
		
		String countQuery = "SELECT  * FROM " + TABLE_COUPON;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		
		System.out.println("no. of records:");
		System.out.println(cursor.getCount());
		cursor.close();
		db.close();
		// return count
		return cursor.getCount();
	}

	
	public void deleteCouponData(Coupondata data) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_COUPON, KEY_ID + " = ?",
                new String[] { String.valueOf(data.getID()) });
        db.close();
	}
	
	public int updateCouponData(Coupondata data)
	{
		 SQLiteDatabase db = this.getWritableDatabase();
		 
	        ContentValues values = new ContentValues();
	        values.put(KEY_START, data.getStartDate());
	        values.put(KEY_END, data.getEndDate());
	 
	        // updating row
	        return db.update(TABLE_COUPON, values, KEY_ID + " = ?",
	                new String[] { String.valueOf(data.getID()) });
		
		
	}

}
