package com.eday.zappscan;

import java.util.Date;

public class Coupon {
	 public String vendor;
     public Date expdate;
     public int discount;
     public String bmpResourceId;

     public Coupon()
         {
             // TODO Auto-generated constructor stub
         }

     public Coupon(String vendor, Date expdate, int discount, String resourceFilePath)
         {
             this.vendor = vendor;
             this.expdate = expdate;
             this.discount= discount;
             this.bmpResourceId = resourceFilePath;
         }

}
