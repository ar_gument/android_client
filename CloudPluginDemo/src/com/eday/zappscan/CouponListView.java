package com.eday.zappscan;


import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class CouponListView extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.coupon_listview);
		
		
		ListAdapter adapter = new ListAdapter(
                getApplicationContext(), R.layout.list_item, SplashActivity.couponList);
         
        // Get reference to ListView holder
        ListView lv = (ListView) findViewById(R.id.listview);
         
        // Set the ListView adapter
        lv.setAdapter(adapter);
        
        lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
        	public void onItemClick(AdapterView<?> parent, View view,
                int position, long id) {
   
   
                // Launching new Activity on selecting single List Item
                Intent i = new Intent(getApplicationContext(), SingleCouponView.class);
                // sending data to new activity
                i.putExtra("image", SplashActivity.couponList.get(position).bmpResourceId);
                startActivity(i);
   
            }
          });
	}
}
