package com.eday.zappscan;

public class Coupondata {
	
	 //private variables
    int _id;
    String _startDate;
    String _endDate;
    String _imageUrl;
    
    public Coupondata(int id, String startDate, String endDate) {
        this._id = id;
        this._startDate = startDate;
        this._endDate = endDate;
    }
    
    public Coupondata( String startDate, String endDate) {
       
        this._startDate = startDate;
        this._endDate = endDate;
    }
    public Coupondata() {
		// TODO Auto-generated constructor stub
	}

	public int getID(){
        return this._id;
    }
     
    // setting id
    public void setID(int id){
        this._id = id;
    }
     
    // getting startDate
    public String getStartDate(){
        return this._startDate;
    }
     
    // setting startDate
    public void setStartDate(String startDate){
        this._startDate = startDate;
    }
     
    // getting end date
    public String getEndDate(){
        return this._endDate;
    }
     
    // setting end date
    public void setEndDate(String endDate){
        this._endDate = endDate;
    }
    
    public void setImageUrl(String imageUrl)
    {
    	this._imageUrl = imageUrl;
    }
    
    public String getImageUrl()
    {
    	return this._imageUrl;
    }

}
