package com.eday.zappscan;

import java.sql.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import com.eday.zappscan.Databasecreator;
import com.eday.zappscan.Coupon;
import com.metaio.cloud.plugin.MetaioCloudPlugin;
import com.metaio.sdk.jni.IMetaioSDKAndroid;

public class SplashActivity extends Activity {
	static {
		IMetaioSDKAndroid.loadNativeLibs();
	}
	private static final String ASSETS_DIR = "/HWUserData/Images/";
	
	public static List<Coupon> couponList = new ArrayList<Coupon>();
	/**
	 * standard tag used for all the debug messages
	 */
	public static final String TAG = "MetaioCloudPluginTemplate";

	/**
	 * Display log messages with debug priority
	 * 
	 * @param msg Message to display
	 * @see Log#d(String, String)
	 */
	public static void log(String msg) {
		if (msg != null)
			Log.d(TAG, msg);

	}
	Databasecreator db; //sqlite
	/**
	 * Progress dialog
	 */
	private ProgressDialog progressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.splashscreen);
		
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			ImageView img = (ImageView) findViewById(R.id.helpImage);
			if(img != null)
				img.setImageResource(R.drawable.helpscreen_landscape);
			
			ImageView img2 = (ImageView) findViewById(R.id.splashImage);
			if(img2 != null)
				img2.setImageResource(R.drawable.splash_landscape);
		}
		 db = new Databasecreator(this); //sqlite
		UpdateCouponList();
		JunaioStarterTask junaioStarter = new JunaioStarterTask();
		junaioStarter.execute(1);
		
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	/**
	 * Launch junaio live view
	 */
	private void launchLiveView() {
		// Set your channel id in /res/values/channelid.xml
		int myChannelId = 0;
		
		if(Locale.getDefault().getDisplayLanguage() == "suomi")
			myChannelId = getResources().getInteger(R.integer.channelid_suomi);
		else
			myChannelId = getResources().getInteger(R.integer.channelid);

		// if you have set a channel ID, then load it directly
		if (myChannelId != -1) {
			startChannel(myChannelId, true);
		}
/*		View wmview = getLayoutInflater().inflate(R.layout.watermark, null);
		addContentView(wmview,  new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));		
*/
	}

	public void startAREL(View v) {
		//startChannel(174470, false);// start AREL test
		launchLiveView();
	}
	

	public void startLb(View v) {
		startChannel(4796, false);// start Wikipedia EN
	}

	public void startChannel(int channelId, boolean andFinishActivity) {
		Intent intent = new Intent(SplashActivity.this, MetaioCloudARViewTestActivity.class);
		intent.putExtra(getPackageName() + ".CHANNELID", channelId);
		startActivity(intent);

		if (andFinishActivity)
			finish();
	}

	private class JunaioStarterTask extends AsyncTask<Integer, Integer, Integer> {

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(SplashActivity.this, "ZappScan", "Starting up...");
		}
 
		@Override
		protected Integer doInBackground(Integer... params) {

			// TODO Set authentication if a private channel is used
			// MetaioCloudPlugin.setAuthentication("username", "password");

			// Start junaio, this will initialize everything the plugin needs
			int result = MetaioCloudPlugin.startJunaio(this, getApplicationContext());

			return result;
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {

		}

		@Override
		protected void onPostExecute(Integer result) {
			if (progressDialog != null) {
				progressDialog.cancel();
				progressDialog = null;
			}

			switch (result) {
			case MetaioCloudPlugin.ERROR_EXSTORAGE:
			case MetaioCloudPlugin.ERROR_INSTORAGE:
			case MetaioCloudPlugin.CANCELLED:
			case MetaioCloudPlugin.ERROR_CPU_NOT_SUPPORTED:
			case MetaioCloudPlugin.ERROR_GOOGLE_SERVICES:
				showDialog(result, null);
				break;
			case MetaioCloudPlugin.SUCCESS:
				launchLiveView();
				break;
			}
		}

	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle bundle) {

		AlertDialog.Builder builder = new Builder(this);
		builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});

		switch (id) {
		case MetaioCloudPlugin.ERROR_EXSTORAGE:
			builder.setMessage("External storage is not available. If you have your USB plugged in, set the USB mode to only charge");
			break;
		case MetaioCloudPlugin.ERROR_INSTORAGE:
			builder.setMessage("Internal storage is not available");
			break;
		case MetaioCloudPlugin.CANCELLED:
			SplashActivity.log("Starting junaio cancelled");
			break;
		case MetaioCloudPlugin.ERROR_CPU_NOT_SUPPORTED:
			SplashActivity.log("CPU is not supported");
			break;
		case MetaioCloudPlugin.ERROR_GOOGLE_SERVICES:
			SplashActivity.log("Google APIs not found");
			break;
		}

		return builder.show();
	}
	
	
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);

		ImageView img = (ImageView) findViewById(R.id.helpImage);
		ImageView img2 = (ImageView) findViewById(R.id.splashImage);

	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	    	if(img != null)
	    		img.setImageResource(R.drawable.helpscreen_landscape);
	    	if(img2 != null)
	    		img2.setImageResource(R.drawable.splash_landscape);
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	    	if(img != null)
	    		img.setImageResource(R.drawable.helpscreentrans);
	    	if(img2 != null)
	    		img2.setImageResource(R.drawable.splash);
	    }
	}
	
	private void UpdateCouponList()
    {
    	Date dt = Date.valueOf("2013-05-30");
    	Date dt2 = Date.valueOf("2014-06-20");
    	Coupon c1 = new Coupon("Ilves", dt, 20, ASSETS_DIR + "ilves3.png");
    	/*Coupon c2 = new Coupon("Stockmann", dt2, 20, ASSETS_DIR + "coupon2.png");
    	Coupon c3 = new Coupon("Verkkokauppa.com", dt, 20, ASSETS_DIR + "coupon3.png");
    	Coupon c4 = new Coupon("Verkkokauppa.com", dt2, 20, ASSETS_DIR + "coupon4.png");*/
    	
    	couponList.add(c1);
    	//couponList.add(c2);
    	//couponList.add(c3);
    	//couponList.add(c4);
    }

}
